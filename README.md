#Laravel Order Management System api.

**Following steps must be follewed**

1. Change the mysql credentials in .env file
2. Update the composer
3. Do the migration php artisan migrate ( php artisan migrate:refresh )
4. give the proper permission for public and storage folder
5. php artisan serve
6. Reigster one user

**In routes/web.php file i have used loginusinguserid method **

###API detatils###
---------------
1. POST /orders  ­ Create order in the system and persist the same in orders and 
order_items table. 
2. PUT /orders/{id} ­ Update order & order item attributes. 
3. PUT /orders/{id}/cancel ­ Cancel the order. 
4. PUT /orders/{id}/payment ­ Add payment to the order 
5. GET /orders/{id} ­ Get order by id 
6. GET /orders/search?user_id=123 ­ Get orders by user 
7. GET /orders/today ­ Get all orders which were created today. 
