<?php

namespace App\Http\Controllers;

use App\Order;
use App\OrderItem;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \I$lluminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        $order = new Order();
        $order->email_id = $request->get('email_id');
        $order->status = $request->get('status');
        $order->user_id = $user->id;
        $order->save();
        $order_id = $order->id;
        $order_item = new OrderItem();
        $order_item->order_id = $order_id;
        $order_item->name = $request->get('name');
        $order_item->price = $request->get('price');
        $order_item->quantity = $request->get('quantity');
        $order_item->save();
        $data['order_id'] = $order_id;
        $data['orderitem_id'] = $order_item->id;

        return response()->json(array('message'=>'Orders& Orders Item Created','data'=>$data),200);

    }

    public function search(){
        $user_id = $_REQUEST['user_id'];
        $order = Order::where('user_id',$user_id)->get();
        foreach ($order as $key=>$data){
            $result[$key]['order_id'] = $data->id;
            $result[$key]['email_id'] = $data->email_id;
            $result[$key]['status'] = $data->status;

        }
        return response()->json(array('data'=>$result),200);

    }

    /*
     * This function used to Cancel the current Order
     */
    public function cancel($id) {

        $order = Order::findOrFail($id);
        $order->status = 'cancelled';
        $order->save();
        return response()->json(array('message'=>'Orders Cancelled Successfully'),200);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = Order::findOrFail($id);
        return response()->json(array('orderId'=>$order->id,'Email'=>$order->email_id,'status'=>$order->status),200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $order = Order::find($id);
        $order->email_id = $request->get('email_id');
        $order->status = $request->get('status');
        $order->save();
        $item = $order->item()->get();
        $Order_item = OrderItem::findOrFail($item[0]->id);
        $Order_item->name = $request->get('name');
        $Order_item->price = $request->get('price');
        $Order_item->quantity = $request->get('quantity');
        $Order_item->save();

        return response()->json(array('message'=>"Order & Order Item updated"),200);

    }

    /*
     * This function used to get the Today order
     */
    public function todayOrder() {
        $today_orders = \App\Order::where('created_at', '>=', Carbon::today()->toDateString())->get();
        foreach ($today_orders as $key=>$order) {
            $data[$key]['order_id'] = $order->id;
            $data[$key]['email_id'] = $order->email_id;
            $data[$key]['status'] = $order->status;

        }
        return response()->json(array('data'=>$data),200);

    }

    public function payment($id) {

        $order = Order::findOrFail($id);
        $order->status = 'processed';
        $order->save();
        return response()->json(array('message'=>'Your payment is processed','order_id'=>$order->id),200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
