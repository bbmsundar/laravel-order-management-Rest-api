<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
     protected $table = 'orders';

     public function item(){

         return $this->hasOne('App\OrderItem');
     }

     public function users_(){

         return $this->hasOne('App\User');
     }
}
